#!/usr/bin/env node

const fs = require('fs');
const formgen = require('./formgen.js');

const USAGE = `NAME
    formgen - a CLI tool to generate HTML forms from model descriptions

SYNOPSIS
    formgen [-a] [--ajax] file
    formgen [-h] [--help]

DESCRIPTION
    formgen generates an HTML form based on the description of a model. The
    given file is simply a JSON file that describes the attributes of the model.
    More specifically, a model file must contain an array of objects, where each
    object in the array describes an attribute of the model.
    
    The following fields must be included in each object:

        name {string} : The name of the attribute
        type {string} : The data type of the attribute. Must be one of the
            following:

            "string",
            "number"

    The following fields are optional:

        formElement {string} : Manually choose which HTML form element to use for this
            attribute. Must be one of the following:

            "textarea"
    
OPTIONS
    -a, --ajax
        Use AJAX to submit the form instead of the default form submission.
    
    -h, --help
        Display this help text`;
const OPTIONS = {
    ajax: false
};
let i = 2;
let arg = process.argv[i];

while (arg !== undefined) {
    if (arg[0] === '-') {
        switch (arg) {
            case '-h':
            case '--help':
                console.log(USAGE);
                process.exit(0);
                break;
            case '-a':
            case '--ajax':
                OPTIONS.ajax = true;
                break;
            default:
                console.log(`Unrecognized command line flag: ${arg}`);
                process.exit(-1);
        }
    } else break;
    i++;
    arg = process.argv[i];
}

const modelFile = process.argv[i];

if (typeof modelFile !== 'string' || modelFile.length < 1) {
    console.log(USAGE);
    process.exit(-1);
}

const fileHandle = fs.readFile(modelFile, 'utf8', (err, data) => {
    if (err) throw err;
    console.log(formgen(JSON.parse(data), OPTIONS));
});
