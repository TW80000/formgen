/**
 * Generate an HTML form from the given model
 * @param {JSON} model The model description
 * @param {Object} options The available options
 *     {Boolean} options.ajax Use AJAX instead of letting the browser post the
 *         form.
 */
function formgen(model, options) {
    let form = `<form action="/YOUR/PATH/HERE" method="post">`;

    for (let i = 0; i < model.length; i++) {
        const attr = model[i];
        form += markupForAttribute(attr);
    }

    if (options.ajax) {
        form += `
    <div>
        <button id="submitBtn" type="button" value="submit">
    </div>
</form>`;
    } else {
        form += `
    <div>
        <input type="submit" value="submit">
    </div>
</form>`;
    }

    if (options.ajax) {
        let script = `<script>
    /**
     * Callback for response from POST of form data
     * @param responseObj {JSON} The response object from the server.
     */
    function handleResponse(responseObj) {
        // YOUR CODE HERE
    }

    const SUBMIT_BTN = document.getElementById('submitBtn');

    SUBMIT_BTN.addEventListener('click', evt => {
        const model = getModelFromForm();
        try {
            const response = await fetch('/YOUR/API/ROUTE/HERE', {
                method: 'POST',
                headers: new Headers({ "Content-Type": "application/json" }),
                body: JSON.stringify(model),
                credentials: "include"
            });
            try {
                const responseObj = await response.json();
                handleResponse(responseObj);
            } catch (err) {
                alert("Couldn't parse response into JSON");
            }
        } catch (err) {
            alert("Error: Request failed");
        }
    });
    
    /** Returns a JSON object containing the form data. */
    function getModelFromForm() {`;
        for (var i = 0; i < model.length; i++) {
            const attr = model[i];
            script += `
        const ${attr.name.toUpperCase()}_ELM = document.getElementById('${attr.name}');`;
        }
        script += `
        return {`;
        for (var i = 0; i < model.length; i++) {
            const attr = model[i];
            script += `
            "${attr.name}": ${attr.name.toUpperCase()}_ELM.value(),`;
        }
        script += `
        };
    }
</script>`;
        return form + script;
    }

    return form;
}

function markupForAttribute(attribute) {
    const inputType = attribute.formElement || attribute.type;
    switch (inputType) {
        case 'string':
            return `
    <div>
        <label for="${attribute.name}">${attribute.name}</label>
        <input id="${attribute.name}" name="${attribute.name}" type="text">
    </div>`;
            break;
        case 'number':
            return `
    <div>
        <label for="${attribute.name}">${attribute.name}</label>
        <input id="${attribute.name}" name="${attribute.name}" type="number">
    </div>`;
            break;
        case 'textarea':
            return `
    <div>
        <label for="${attribute.name}">${attribute.name}</label>
        <textarea id="${attribute.name}" name="${attribute.name}"></textarea>
    </div>`;
            break;
    }
}

module.exports = formgen;
