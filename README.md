# formgen

A CLI tool to generate HTML forms from model descriptions.

formgen is very, very early in development and is missing a lot of obvious functionality.

## Installation

```bash
npm i -g @tw80000/formgen
```

## Usage

```
NAME
    formgen - a CLI tool to generate HTML forms from model descriptions

SYNOPSIS
    formgen [-a] [--ajax] file
    formgen [-h] [--help]

DESCRIPTION
    formgen generates an HTML form based on the description of a model. The
    given file is simply a JSON file that describes the attributes of the model.
    More specifically, a model file must contain an array of objects, where each
    object in the array describes an attribute of the model.
    
    The following fields must be included in each object:

        name {string} : The name of the attribute
        type {string} : The data type of the attribute. Must be one of the
            following:

            "string",
            "number"

    The following fields are optional:

        formElement {string} : Manually choose which HTML form element to use for this
            attribute. Must be one of the following:

            "textarea"
    
OPTIONS
    -a, --ajax
        Use AJAX to submit the form instead of the default form submission.
    
    -h, --help
        Display this help text
```
    